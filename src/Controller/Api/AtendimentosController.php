<?php

/*
 * This file is part of the Novo SGA project.
 *
 * (c) Rogerio Lino <rogeriolino@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Controller\Api;

use DateTime;
use Exception;

use Novosga\Http\Envelope;
use Novosga\Entity\Lotacao;
use Novosga\Entity\Servico;
use Novosga\Entity\Unidade;
use Novosga\Entity\Usuario;
use Novosga\Entity\Atendimento;
use App\Service\SecurityService;
use MacClienteSocketIO\SocketIO;
use Novosga\Service\FilaService;
use Novosga\Service\UsuarioService;
use Novosga\Service\AtendimentoService;
use App\Controller\Api\ApiCrudController;
use Novosga\Event\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * AtendimentosController
 *
 * @author Rogério Lino <rogeriolino@gmail.com>
 *
 * @Route("/api/atendimentos")
 */
class AtendimentosController extends ApiCrudController
{
    use Actions\GetTrait,
        Actions\FindTrait;

    public function getEntityName()
    {
        return \Novosga\Entity\Atendimento::class;
    }

    /**
     *
     * @param Request $request
     * @return Response
     *
     * @Route("/setLocal", name="api_setlocal", methods={"POST"})
     */
    public function setLocal(
        Request $request,
        UsuarioService $usuarioService,
        EventDispatcherInterface $dispatcher,
        TranslatorInterface $translator
    ) {
        $envelope = new Envelope();        

        try {
            $data   = json_decode($request->getContent());
            $numero = isset($data->numeroLocal) ? (int) $data->numeroLocal : 0;
            $tipo   = isset($data->tipoAtendimento) ? $data->tipoAtendimento : FilaService::TIPO_TODOS;
            
            if ($numero <= 0) {
                throw new Exception(
                    $translator->trans('error.place_number', [], self::DOMAIN)
                );
            }
            
            if ($numero <= 0) {
                throw new Exception(
                    $translator->trans('error.queue_type', [], self::DOMAIN)
                );
            }

            //$usuario = $this->getUser();
            //$unidade = $usuario->getLotacao()->getUnidade();
            $em = $this->getDoctrine()->getManager();
            $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());        
            $lotacao = $em->getRepository(Lotacao::class)->findOneBy(["usuario" =>$usuario->getId()]);
            $unidade = $lotacao->getUnidade();

            $dispatcher->createAndDispatch(
                'novosga.attendance.pre-setlocal',
                [$unidade, $usuario, $numero, $tipo],
                true
            );

            $m1 = $usuarioService->meta($usuario, UsuarioService::ATTR_ATENDIMENTO_LOCAL, $numero);
            $m2 = $usuarioService->meta($usuario, UsuarioService::ATTR_ATENDIMENTO_TIPO, $tipo);

            $dispatcher->createAndDispatch(
                'novosga.attendance.setlocal',
                [$unidade, $usuario, $numero, $tipo],
                true
            );

            $envelope->setData([
                'numero' => $m1,
                'tipo'   => $m2,
            ]);
        } catch (Exception $e) {
            $envelope->exception($e);
        }

        return $this->json($envelope);
    }

    /**
     * Retorna os usuários que atende o serviço na unidade
     *
     * @param Novosga\Request $request
     *
     * @Route("/meuLocal", name="api_usuario_atendimento", methods={"GET"})
     */
    public function meuLocal(
        Request $request, 
        TranslatorInterface $translator,
        UsuarioService $usuarioService)
    {
        $envelope       = new Envelope();
        $em = $this->getDoctrine()->getManager();
        $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());        
        $local = $this->getNumeroLocalAtendimento($usuarioService, $usuario);
                        
        $envelope->setData($local);      
        return $this->json($envelope);
    }

    /**
     * @Route("/chamarSenha",name="chamarSenha", methods={"GET"})
     */
    public function chamarSenha(
        Request $request,
        AtendimentoService $atendimentoService,
        FilaService $filaService,
        UsuarioService $usuarioService,
        TranslatorInterface $translator,
        SecurityService $securityService
    ) {


        $em = $this->getDoctrine()->getManager();
        //$json = $request->getContent();
        //$object = json_decode($json, true);
        //$senha = ($object) ? $object['sigla'] .'-'.$object['numero']: null;
        
        $sigla = $request->get('sigla');
        $numero = $request->get('numero');
        $senha = ($sigla && $numero) ? $sigla .'-'.$numero: null;
        //return $this->json(["senha"=>$senha]);

        $envelope = new Envelope();

        $attempts = 5;
        $proximo = null;
        $success = false;
        $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());        
        $lotacao = $em->getRepository(Lotacao::class)->findOneBy(["usuario" =>$usuario->getId()]);
        $unidade = $lotacao->getUnidade();

        // verifica se ja esta atendendo alguem
        $atual = $atendimentoService->atendimentoAndamento($usuario->getId(), $unidade);
        //return $this->json($atual);

        // se ja existe um atendimento em andamento (chamando senha novamente)
        if ($atual) {
            $success = true;
            $proximo = $atual;
        } else {
            $local    = $this->getNumeroLocalAtendimento($usuarioService, $usuario);
            $servicos = $usuarioService->servicos($usuario, $unidade);

            do {
                $tipo = $this->getTipoAtendimento($usuarioService, $usuario);                
                $atendimentos = $filaService->filaAtendimento($unidade, $usuario, $servicos, $tipo, 1, $senha);                   
                if (count($atendimentos)) {
                    $proximo = $atendimentos[0];
                    //return $this->json($atendimentos);
                    $success = $atendimentoService->chamar($proximo, $usuario, $local);
                    if (!$success) {
                        usleep(100);
                    }
                    --$attempts;
                } else {
                    // nao existe proximo
                    break;
                }
            } while (!$success && $attempts > 0);
        }
        
        if (!$success) {
            $data=["code"=> 266, "error"=>"Sem senhas para chamar"];
            $envelope->setData($data);
            return $this->json($envelope);
        }

        // response
        if (!$success) {
            if (!$proximo) {
                throw new Exception(
                    $translator->trans('error.queue.empty', [], self::DOMAIN)
                );
            } else {
                throw new Exception(
                    $translator->trans('error.attendance.in_process', [], self::DOMAIN)
                );
            }
        }

        $atendimentoService->chamarSenha($unidade, $proximo);        
        $data = $proximo->jsonSerialize();
        $envelope->setData($data);

        // socket
        $client = new SocketIO('localhost', 2020);
        $client->setQueryParams([
            'token' => 'edihsudshuz',
            'id' => '8780',
            'cid' => '344',
            'cmp' => 2339
        ]);

        $success = $client->emit('register user', [
            'secret' => $securityService->getWebsocketSecret(),
            'user' =>  $this->getUser()->getId(),
            'unity' => $unidade->getId()
        ]);

        $success = $client->emit('call ticket', [
            'unity' => $unidade->getId(),
            'service' => $data['servico']['id'],
            'hash' => $data['hash']
        ]);
        // socket

       return $this->json($envelope);

    }

    /**
     * Inicia o atendimento com o proximo da fila.
     *
     * @param Novosga\Request $request
     *
     * @Route("/iniciaAtendimento", name="novosga_attendance_iniciar_atendimento", methods={"GET"})
     */
    public function iniciarAtendimento(
        Request $request,
        AtendimentoService $atendimentoService,
        TranslatorInterface $translator
    ) {

        $em = $this->getDoctrine()->getManager();
        //$json = $request->getContent();
        //$object = json_decode($json, true);
        //$senha = ($object) ? $object['sigla'] .'-'.$object['numero']: null;

        $sigla = $request->get('sigla');
        $numero = $request->get('numero');
        $senha = ($sigla && $numero) ? $sigla .'-'.$numero: null;

        $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());        
        $lotacao = $em->getRepository(Lotacao::class)->findOneBy(["usuario" =>$usuario->getId()]);
        $unidade = $lotacao->getUnidade();
        
        $atual   = $atendimentoService->atendimentoAndamento($usuario->getId(), $unidade, $senha);
        //return $this->json($atual);

        if ($atual == null && $senha !=null){
            $data=["code"=> 404, "error"=>"Esta senha não esta direcionada a você ou não foi encontrada"];
            $envelope = new Envelope();
            $envelope->setData($data);
            return $this->json($envelope);
        }

        if (!$atual) {
            throw new Exception(
                $translator->trans('error.attendance.empty', [], self::DOMAIN)
            );
        }

        $atendimentoService->iniciarAtendimento($atual, $usuario);

        $data     = $atual->jsonSerialize();
        $envelope = new Envelope();
        $envelope->setData($data);

        return $this->json($envelope);
        //return $this->json(["mensagem" => $atual]);

    }


    /**
     * Marca o atendimento como nao compareceu.
     *
     * @param Novosga\Request $request
     *
     * @Route("/naoCompareceu", name="novosga_attendance_naocompareceu_atendimento", methods={"GET"})
     */
    public function naoCompareceu(
        Request $request,
        AtendimentoService $atendimentoService,
        TranslatorInterface $translator
    ) {

        $em = $this->getDoctrine()->getManager();
        //$json = $request->getContent();

        $sigla = $request->get('sigla');
        $numero = $request->get('numero');
        $senha = ($sigla && $numero) ? $sigla .'-'.$numero: null;

        $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());
        $lotacao = $em->getRepository(Lotacao::class)->findOneBy(["usuario" =>$usuario->getId()]);
        $unidade = $lotacao->getUnidade();
        //$object = json_decode($json, true);
        //$senha = ($object) ? $object['sigla'] .'-'.$object['numero']: null;

        $atual   = $atendimentoService->atendimentoAndamento($usuario->getId(), $unidade, $senha);
        //return $this->json($atual);

        if ($atual == null && $senha !=null){
            $data=["code"=> 404, "error"=>"Esta senha não esta direcionada a você ou não foi encontrada"];
            $envelope = new Envelope();
            $envelope->setData($data);
            return $this->json($envelope);
        }

        if (!$atual) {            
            throw new Exception(
                $translator->trans('error.attendance.empty', [], self::DOMAIN)                
            );
        }
        
        $atendimentoService->naoCompareceu($atual, $usuario);

        $data     = $atual->jsonSerialize();
        $envelope = new Envelope();
        $envelope->setData($data);

        return $this->json($envelope);
    }


    /**
     * Marca o atendimento como encerrado.
     *
     * @param Novosga\Request $request
     *
     * @Route("/encerraAtendimento", name="novosga_attendance_encerrar_atendimento", methods={"POST"})
     */
    public function encerrar(
        Request $request,
        AtendimentoService $atendimentoService,
        TranslatorInterface $translator
    ) {

        $em = $this->getDoctrine()->getManager();
        $envelope = new Envelope();
        //$data     = json_decode($request->getContent());
        $data     = json_decode($request->getContent());

        $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());
        $lotacao = $em->getRepository(Lotacao::class)->findOneBy(["usuario" =>$usuario->getId()]);
        $unidade = $lotacao->getUnidade();
        
        $atual   = $atendimentoService->atendimentoAndamento($usuario->getId(), $unidade);

        if (!$atual) {
            throw new Exception(
                $translator->trans('error.attendance.not_in_process', [], self::DOMAIN)
            );
        }

        $servicos   = explode(',', $data->servicos);
        $resolucao  = $data->resolucao;
        $observacao = $data->observacao;

        if (empty($servicos)) {
            throw new Exception(
                $translator->trans('error.attendance.no_service', [], self::DOMAIN)
            );
        }


        $novoUsuario = null;
        $servicoRedirecionado = null;

        if ($data->redirecionar) {
            if (isset($data->novoServico)) {
            $servicoRedirecionado = $em
                ->getRepository(Servico::class)
                ->find($data->novoServico);
            }
            if (isset($data->novoUsuario)) {
                $novoUsuario = $em
                    ->getRepository(Usuario::class)
                    ->find($data->novoUsuario);
            }
        }

        if (in_array($resolucao, [AtendimentoService::RESOLVIDO, AtendimentoService::PENDENTE])) {
            $atual->setResolucao($resolucao);
        }

        if ($observacao) {
            $atual->setObservacao($observacao);
        }

        $atendimentoService->encerrar($atual, $unidade, $servicos, $servicoRedirecionado, $novoUsuario);

        //return $this->json(array('mensagem'=> $servicos));
        $envelope->setData($atual);
        return $this->json($envelope);
        //$servicos   = explode(',', $data->servicos);
        //return $this->json(array('mensagem'=> $servicos));
    }


    /**
     *
     * @param Request $request
     * @return Response
     *
     * @Route("/listaAtendimentos", name="novosga_attendance_lista_atendimentos", methods={"GET"})
     */
    public function listaAtendimentos(
        Request $request,
        FilaService $filaService,
        UsuarioService $usuarioService
    ) {

        $em = $this->getDoctrine()->getManager();
        $json = $request->getContent();

        $usuario  = $em->getRepository(Usuario::class)->find($this->getUser()->getId());
        $lotacao = $em->getRepository(Lotacao::class)->findOneBy(["usuario" =>$usuario->getId()]);
        $unidade = $lotacao->getUnidade();
        
        $envelope = new Envelope();
        
        $local    = $this->getNumeroLocalAtendimento($usuarioService, $usuario);
        $tipo     = $this->getTipoAtendimento($usuarioService, $usuario);
        
        $servicos     = $usuarioService->servicos($usuario, $unidade);
        $atendimentos = $filaService->filaAtendimento($unidade, $usuario, $servicos, $tipo);
        
        // fila de atendimento do atendente atual
        $data = [
            'atendimentos' => $atendimentos,
            'usuario'      => [
                'numeroLocal'     => $local,
                'tipoAtendimento' => $tipo,
            ],
        ];

        $envelope->setData($data);

        return $this->json($envelope);
    }

    private function getNumeroLocalAtendimento(UsuarioService $usuarioService, Usuario $usuario)
    {
        $numeroLocalMeta = $usuarioService->meta($usuario, UsuarioService::ATTR_ATENDIMENTO_LOCAL);
        $numero = $numeroLocalMeta ? (int) $numeroLocalMeta->getValue() : null;

        return $numero;
    }

    private function getTipoAtendimento(UsuarioService $usuarioService, Usuario $usuario)
    {
        $tipoAtendimentoMeta = $usuarioService->meta($usuario, UsuarioService::ATTR_ATENDIMENTO_TIPO);
        $tipoAtendimento = $tipoAtendimentoMeta ? $tipoAtendimentoMeta->getValue() : FilaService::TIPO_TODOS;

        return $tipoAtendimento;
    }
}
